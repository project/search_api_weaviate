# Search API Weaviate

## INTRODUCTION

This module provides a Weaviate.io backend for the
[Search API](https://www.drupal.org/project/search_api) module.

The backend uses the [Weaviate.io](https://www.weaviate.io/) an open-source 
vector database. It allows you to store data objects and vector embeddings from 
your favorite ML-models, and scale seamlessly into billions of data objects.

## REQUIREMENTS

1. [Search API](https://www.drupal.org/project/search_api) module.
2. [Weaviate.io SDK for PHP](https://github.com/timkley/weaviate-php) library.

## INSTALLATION

The module can be installed via the
[standard Drupal installation process](https://drupal.org/node/1897420).

## CONFIGURATION

See [Search API module's README](https://www.drupal.org/node/2852816) for
instructions.
