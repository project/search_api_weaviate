<?php

namespace Drupal\search_api_weaviate\Plugin\search_api\backend;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\search_api\Backend\BackendPluginBase;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\SearchApiException;
use Weaviate\Weaviate;

/**
 * Indexes items using the Weaviate.io API.
 *
 * @SearchApiBackend(
 *   id = "weaviate",
 *   label = @Translation("Weaviate"),
 *   description = @Translation("Indexes items using the Weaviate API.")
 * )
 */
class SearchApiWeaviateBackend extends BackendPluginBase implements PluginFormInterface {

  use PluginFormTrait;

  /**
   * Weaviate API client instance.
   *
   * @var \Probots\Weaviate\Client
   */
  protected $weaviateClient;

  /**
   * The Weaviate Index ID.
   *
   * @var string
   */
  protected $weaviateIndexId;

  /**
   * The key repository.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $plugin = new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $plugin->keyRepository = $container->get('key.repository');

    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'key' => '',
      'cluster_url' => '',
      'additional_headers' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $weaviate_console_link = Link::fromTextAndUrl('Weaviate dashboard', Url::fromUri('https://console.weaviate.cloud/dashboard'))
      ->toString();

    $form['key'] = [
      '#type' => 'key_select',
      '#title' => $this->t('API key'),
      '#description' => $this->t('To find your Weaviate API key, open the @weaviate_console_link and click on Authentication
        in your cluster details page.', [
          '@weaviate_console_link' => $weaviate_console_link,
        ]),
      '#default_value' => $this->configuration['key'],
      '#empty_value' => '',
      '#empty_label' => '',
      '#required' => TRUE,
    ];

    $form['cluster_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cluster URL'),
      '#description' => $this->t('To find your cluster URL, open the @weaviate_console_link and copy the URL.', [
        '@weaviate_console_link' => $weaviate_console_link,
      ]),
      '#default_value' => $this->configuration['cluster_url'],
      '#required' => TRUE,
    ];

    $form['additional_headers'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Additional Headers'),
      '#default_value' => $this->configuration['additional_headers'],
      '#description' => $this->t("Additional Headers for example if you want to use the inference service API 
        to generate vectors, you must provide an additional inference API key in the header."),
      '#attributes' => [
        'placeholder' => $this->t('"X-Cohere-Api-Key": "YOUR-COHERE-API-KEY",  // For Cohere
        "X-HuggingFace-Api-Key": "YOUR-HUGGINGFACE-API-KEY",  // For Hugging Face
        "X-OpenAI-Api-Key": "YOUR-OPENAI-API-KEY",  // For OpenAI
        "X-Palm-Api-Key": "YOUR-PALM-API-KEY",  // For PaLM'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    try {
      if ($weaviate_client = $this->getWeaviateClient()) {
        $response = $weaviate_client->index()->list();
      }
    }
    catch (\Exception $e) {
      $message = $this->t('Wrong key or cluster url: @message', [
        '@message' => $e->getMessage(),
      ]);
      $form_state->setErrorByName('key', $message);
      $form_state->setErrorByName('cluster_url', $message);
      $form_state->setErrorByName('additional_headers', $message);

    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['key'] = $form_state->getValue('key');
    $this->configuration['cluster_url'] = $form_state->getValue('cluster_url');
    $this->configuration['additional_headers'] = $form_state->getValue('additional_headers');
  }

  /**
   * {@inheritdoc}
   */
  public function viewSettings() {
    $info[] = [
      'label' => $this->t('Weaviate.io API'),
      'info' => Json::encode($this->configuration),
    ];
    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public function addIndex(IndexInterface $index) {
    if ($weaviate_client = $this->getWeaviateClient()) {
      $index_id = $this->getWeaviateIndexId($index->id());
      if ($weaviate_client->schema()->get()->getClasses()->isEmpty()) {
        $weaviate_client->schema()->create([
          'class' => $index->id(),
          'description' => 'Created by Search API',
          'vectorizer' => 'none',
          'properties' => [
            ['dataType' => ['string'], 'name' => '_search_api_doc_source_id'],
            ['dataType' => ['string'], 'name' => '_search_api_doc_source_type'],
            ['dataType' => ['int'], 'name' => '_search_api_doc_chunk_index'],
            ['dataType' => ['text'], 'name' => '_search_api_doc_content'],
            ['dataType' => ['text'], 'name' => '_search_api_doc_metadata'],
          ],
        ]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updateIndex(IndexInterface $index) {
    if ($weaviate_client = $this->getWeaviateClient()) {
      $settings = $index->getThirdPartySettings('search_api_weaviate');
      if (!empty($settings)) {
        $replicas = $settings['replicas'];
        $pod_type = $settings['pod_type'];

        $response = $weaviate_client->index($this->getWeaviateIndexId($index->id()))->configure($pod_type, $replicas);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function removeIndex($index) {
    if (is_object($index)) {
      try {
        if ($weaviate_client = $this->getWeaviateClient()) {
          $response = $weaviate_client->index($this->getWeaviateIndexId($index->id()))->delete();
        }
      }
      catch (\Exception $e) {
        throw new SearchApiException($e->getMessage(), $e->getCode(), $e);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function indexItems(IndexInterface $index, array $items) {
    $indexed = [];

    foreach ($items as $id => $item) {
      try {
        $index = $item->getIndex();
        $vectors = [];
        $metas = [];
        $objects = [];

        foreach ($item->getFields() as $field) {
          if ($field->getType() === 'embedding') {
            foreach ($field->getValues() as $delta => $values) {
              $metas[] = $values['item'];
              $vectors[] = $values['vector'];
            }
          }

          $objects[] = [
            'id' => \Drupal::service('uuid')->generate(),
            'class' => $this->getWeaviateIndexId($index_id),
            'vector' => $vectors,
            'properties' => $metas,
          ];
        }

        $this->client->batch()->create($objects);
        $indexed[] = $id;
      }
      catch (\Exception $e) {
        $this->getLogger()->error($e->getMessage());
      }
    }

    return $indexed;
  }

  /**
   * Indexes a single item on the specified index.
   *
   * @param \Drupal\search_api\Item\ItemInterface $item
   *   The item to index.
   *
   * @throws \Drupal\search_api\SearchApiException
   *   If the Weaviate.io API error occurs.
   */
  protected function indexItem(ItemInterface $item) {
    try {
      if ($weaviate_client = $this->getWeaviateClient()) {
        $index = $item->getIndex();
        $vectors = [];
        $metas = [];

        foreach ($item->getFields() as $field) {
          if ($field->getType() === 'embedding') {
            foreach ($field->getValues() as $delta => $values) {
              $metas[] = $values['item'];
              $vectors[] = $values['vector'];
            }
          }
        }

        $this->client->objects()->create([
          'id' => \Drupal::service('uuid')->generate(),
          'class' => $this->getWeaviateIndexId($index_id),
          'vector' => $vectors,
          'properties' => $metas,
        ]);
      }
    }
    catch (\Exception $e) {
      throw new SearchApiException($e->getMessage(), $e->getCode(), $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function search(QueryInterface $query) {
    // @todo .
  }

  /**
   * {@inheritdoc}
   */
  public function deleteItems(IndexInterface $index, array $item_ids) {
    try {
      if ($weaviate_client = $this->getWeaviateClient()) {
        $response = $weaviate->index($this->getWeaviateIndexId($index->id()))->vectors()->delete($item_ids);
      }
    }
    catch (\Exception $e) {
      throw new SearchApiException($e->getMessage(), $e->getCode(), $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAllIndexItems(IndexInterface $index, $datasource_id = NULL) {
    try {
      if ($weaviate_client = $this->getWeaviateClient()) {
        $weaviate_client->schema()->delete($this->getWeaviateIndexId($index_id));
      }
    }
    catch (\Exception $e) {
      throw new SearchApiException($e->getMessage(), $e->getCode(), $e);
    }
  }

  /**
   * Helper function to obtain the Weaviate API key.
   *
   * @return string
   *   The Weaviate API key.
   */
  protected function getWeaviateApiKey() {
    if ($key = $this->configuration['key']) {
      if ($key_object = $this->keyRepository->getKey($key)) {
        return $key_object->getKeyValue();
      }
    }
  }

  /**
   * Helper function to gets the Weaviate client.
   *
   * @return \Probots\Weaviate\Client
   *   The Weaviate client.
   */
  protected function getWeaviateClient() {
    if (!isset($this->weaviateClient)) {
      try {
        if ($api_key = $this->getWeaviateApiKey()) {
          $cluster_url = $this->configuration['cluster_url'];
          $additional_headers = json_decode($this->configuration['additional_headers']);
          $this->weaviateClient = new Weaviate($cluster_url, $api_key, $additional_headers);
        }
      }
      catch (\Exception $e) {
        $message = t('There was a problem connecting to the Weaviate API please check your credentials: @message', [
          '@message' => $e->getMessage(),
        ]);

        throw new \Exception($message);
      }
    }

    return $this->weaviateClient;
  }

  /**
   * Helper function to get the Weaviate Index ID.
   *
   * @param string $index_id
   *   The Search API index ID.
   *
   * @return string
   *   The Weaviate Index ID.
   */
  protected function getWeaviateIndexId($index_id) {
    if (!$this->weaviateIndexId) {
      $this->weaviateIndexId = lcfirst(str_replace('_', '', ucwords($index_id, '_')));
    }

    return $this->weaviateIndexId;
  }

}
